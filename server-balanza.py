#!/usr/bin/env python
# -*- coding: utf-8 -*-

###########################################
# Programa servidor de datos de balanzas. #
###########################################
#
#El mismo contesta peticiones GET a la URL (IP):8003/medir con un JSON del siguiente formato:
#{
#    "balanzaEntrada": {
#        "pb": 32438, 
#        "pn": 31938, 
#        "tara": 500
#    }, 
#    "balanzaSalida": {
#        "pb": 22295, 
#        "pn": 21595, 
#        "tara": 700
#    }
#}
#Caso que haya un problema contesta con el siguiente formato:
#
#"{'error': 'Aqui va la descripcion del error'}"
#
#Al implementar los clientes JS utilizar solamente el dato del peso bruto medido, ya sea de ingreso o egreso de camion.
#Habrá una PC extra con un cliente odoo que permitirá realizar ingreso a la vez que en la PC servidor
#se registra un egreso desde su cliente odoo.


from BaseHTTPServer import HTTPServer
from BaseHTTPServer import BaseHTTPRequestHandler
import cgi

#Para testing de pesada
from random import random
from time import sleep

#Para obtencion de pesada
#import serial
import json

PORT = 8004
FILE_PREFIX = "."
LOCAL_IP = "0.0.0.0"


def get_measurements():
    """Dummy function que simula la medicion de las balanzas."""
    medicionEntrada = int(32100+random()*500)
    medicionSalida = int(22000+random()*500)
    sleep(0.7)
    return { 'balanzaEntrada':{'pb':medicionEntrada,'tara':500,'pn':medicionEntrada-500},
             'balanzaSalida':{'pb':medicionSalida,'tara':700,'pn':medicionSalida-700} }


def _get_measurements():
    """WIP: Funcion que obtiene las mediciones del thread que consulta las balanzas"""
    medicionEntrada = -1
    medicionSalida = -1    
    #Envio query a cada balanza y leo sus resultados.
    #Formato de los datos recibidos de la balanza
    #'0x3A',(2 bytes ascci num balanza),'0x7c',(Peso bruto en ascii con puntos 6?),'0x7c',
    #(Valor de tara en ascii con puntos6?),'0x7c',(Peso neto en ascii con puntos6?),'0x7c',(2 bytes de checksum suma saturada),'0D'     
    serEnt.Read()
    #Recibo por pyseiral los datos de ambas balanzas
    return { 'balanzaEntrada':{'pb':medicionEntrada,'tara':500,'pn':medicionEntrada-500},
             'balanzaSalida':{'pb':medicionSalida,'tara':700,'pn':medicionSalida-700} }


if __name__ == "__main__":
    try:
        import argparse
        parser = argparse.ArgumentParser(description='A simple fake server for testing your API client.')
        parser.add_argument('-p', '--port', type=int, dest="PORT",
                           help='the port to run the server on; defaults to 8003')
        parser.add_argument('--path', type=str, dest="PATH",
                           help='the folder to find the json files')

        args = parser.parse_args()

        if args.PORT:
            PORT = args.PORT
        if args.PATH:
            FILE_PREFIX = args.PATH

#        #Configuracion de puerto 1 (Balanza entrada)
#        serEnt = serial.Serial(
#            port='/dev/ttyUSB0',
#            baudrate=9600,
#            parity=serial.PARITY_ODD,
#            stopbits=serial.STOPBITS_ONE,
#            bytesize=serial.EIGHTBITS )
#        #Configuracion de puerto 2 (Balanza salida)
#        serSal = serial.Serial(
#            port='/dev/ttyUSB1',
#            baudrate=9600,
#            parity=serial.PARITY_ODD,
#            stopbits=serial.STOPBITS_ONE,
#            bytesize=serial.EIGHTBITS )
#
#        #Inicializo ambos puertos serie
#        serEnt.open()
#        serEnt.isOpen()
#        serSal.open()
#        serSal.isOpen()


    except Exception:
        # Hubo algun problema... TODO: CAMBIAR ESTO!!!
        pass



class JSONRequestHandler (BaseHTTPRequestHandler):

    def do_GET(self):

        #send response code:
        self.send_response(200)
        #send headers:
        self.send_header("Content-type", "application/json")
        # send a blank line to end headers:
        self.wfile.write("\n")

        #Obtengo los valores mas actuales de medicion
        data=get_measurements()

        #Realizo una lectura de la balanza
        try:
            if  self.path[1:].startswith("medir"):
                jsonp_callback = self.path.split('?')[1].split('&')[0].split('=')[1]
                output = json.dumps(data,indent=4,sort_keys=True)
                output = '%s(%s)' % (jsonp_callback, output)
            else:
                output = "{'error': 'Comando no existente'}"
        except Exception, e:
            output = "{'error': 'Exception: %s'}" % e
        finally:
            self.wfile.write(output)

    def do_POST(self):
        if self.path == "/success":
            response_code = 200
        elif self.path == "/error":
            response_code = 500
        else:
            try:
                response_code = int(self.path[1:])
            except Exception:
                response_code = 201
            
        try:
            self.send_response(response_code)
            self.wfile.write('Content-Type: application/json\n')
            self.wfile.write('Client: %s\n' % str(self.client_address))
            self.wfile.write('User-agent: %s\n' % str(self.headers['user-agent']))
            self.wfile.write('Path: %s\n' % self.path)
            
            self.end_headers()


            form = cgi.FieldStorage(
                    fp=self.rfile, 
                    headers=self.headers,
                    environ={'REQUEST_METHOD':'POST',
                                     'CONTENT_TYPE':self.headers['Content-Type'],
                                     })

            self.wfile.write('{\n')
            first_key=True
            for field in form.keys():
                    if not first_key:
                        self.wfile.write(',\n')
                    else:
                        self.wfile.write('\n')
                        first_key=False
                    self.wfile.write('"%s":"%s"' % (field, form[field].value))
            self.wfile.write('\n}')
                            
        except Exception as e:
            self.send_response(500)


server = HTTPServer((LOCAL_IP, PORT), JSONRequestHandler)
server.serve_forever()
